#ifndef DEKANAT
#define DEKANAT

#include "group.h"
#include <fstream>


class Dekanat
{
public:
    Dekanat();
    void loadGroups(std::string path);
    void loadStudents(std::string path);
    void distributeToGroups();
    void print();
	void printTheBest();
	void printHeads();
	void printStatisticGroups();
	void printStatisticStudents();
    void setMarks();
    void removeStud(int id);
    void removeBedStudents();
    void nextCourse();
    void saveStud();
	void saveGroups();
	void setHeads();
	void moveStud(int id, string destTitle);
	Student* bestStudent();
	Group* bestGroup();

private:

    vector<Group> gr;
    vector<Student> st;
    int findStud(int id);
	int findGroup(string title);
	ofstream logs;
};

#endif // DEKANAT

